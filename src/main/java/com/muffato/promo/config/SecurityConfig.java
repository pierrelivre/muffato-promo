package com.muffato.promo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.muffato.promo.security.CustomUserDetailsService;

@Configuration
@EnableOAuth2Client
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception { 
        auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
    }
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
			.antMatchers("/layout/**")
			.antMatchers("/images/**");
	}
	
	 @Override
	 protected void configure(HttpSecurity http) throws Exception {
		 http
		 	.authorizeRequests()
		 		.antMatchers("/promocoes/nova").hasRole("CADASTRAR_PROMOCAO")
		 		.antMatchers("/promocoes").hasRole("PESQUISAR_PROMOCAO")
		 	.anyRequest().authenticated()
		 .and()
		 	.formLogin()
		 	.loginPage("/login")
		 	.permitAll()
		 .and()
		 	.logout()
		 	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	 	.and()
		 	.csrf().disable()
		 	.sessionManagement()
		 	.invalidSessionUrl("/login");
		}

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}