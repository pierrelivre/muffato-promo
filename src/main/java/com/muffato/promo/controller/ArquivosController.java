package com.muffato.promo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import com.muffato.promo.dto.ArquivoDTO;
import com.muffato.promo.storage.ArquivoStorage;
import com.muffato.promo.storage.ArquivoStorageRunnable;

@RestController
@RequestMapping("/arquivos")
public class ArquivosController {

	@Autowired
	private ArquivoStorage arquivoStorage;
	
	@PostMapping
	@SuppressWarnings("static-access")
	public DeferredResult<ArquivoDTO> upload(@RequestParam("files[]") MultipartFile[] files) {
		DeferredResult<ArquivoDTO> resultado = new DeferredResult<ArquivoDTO>();
		
		Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, this.arquivoStorage));
		try {
			thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		thread.start();
		
		return resultado;
	}
	
	@GetMapping("/temp/{nome}")
	public HttpEntity<byte[]> recuperarArquivoTemporario(@PathVariable("nome") String nome) {
		return recuperarArquivo(nome, this.arquivoStorage.recuperarArquivoTemporario(nome));
	}
	
	@GetMapping("/{nome}")
	public HttpEntity<byte[]> recuperarArquivo(@PathVariable("nome") String nome) {
		return recuperarArquivo(nome, this.arquivoStorage.recuperarArquivo(nome));
	}
	
	@DeleteMapping("/{nome}")
	public void excluirArquivo(@PathVariable("nome") String nomeArquivo) {
		this.arquivoStorage.excluirArquivo(nomeArquivo);
	}
	
	private HttpEntity<byte[]> recuperarArquivo(String nome, byte[] arquivo) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Content-Disposition", "attachment;filename=\"" + nome + "\"");
		HttpEntity<byte[]> entity = new HttpEntity<byte[]>( arquivo, httpHeaders);
		return entity;
	}
}
