package com.muffato.promo.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

	@RequestMapping("/") 
	public String aoLogar() {
		return "redirect:/promocoes";
	}
	
	@RequestMapping("/login")
	public String login(@AuthenticationPrincipal User user) {
		if (user != null) {
			return "redirect:/promocoes";
		}
		return "Login";
	}
	
	@GetMapping("/403")
	public String accessoNegado() {
		return "403";
	}
}
