package com.muffato.promo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.muffato.promo.controller.page.PageWrapper;
import com.muffato.promo.model.Promocao;
import com.muffato.promo.repository.Promocoes;
import com.muffato.promo.repository.filter.PromocaoFilter;
import com.muffato.promo.service.CadastroPromocaoService;
import com.muffato.promo.service.exception.DataInicioPromocaoInvalida;
import com.muffato.promo.service.exception.DatasPromocaoInvalidas;
import com.muffato.promo.service.exception.NomePromocaoJaCadastradoException;

@Controller
@RequestMapping("/promocoes")
public class PromocoesController {

	@Autowired
	private Promocoes promocoes;
	@Autowired
	private CadastroPromocaoService cadastroPromocaoService;
	
	@RequestMapping("/nova")
	public ModelAndView novo(Promocao promocao) {
		ModelAndView mv = new ModelAndView("promocao/CadastroPromocao");
		return mv;
	}
	
	@PostMapping({ "/nova", "{\\+d}" })
	public ModelAndView salvar(@Valid Promocao promocao, BindingResult result, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(promocao);
		}
		
		try {
			this.cadastroPromocaoService.salvar(promocao);
		} catch (NomePromocaoJaCadastradoException nomeJaCadastrado) {
			result.rejectValue("nome", nomeJaCadastrado.getMessage(), nomeJaCadastrado.getMessage());
			return novo(promocao);
		} catch (DatasPromocaoInvalidas datasInvalidas) {
			result.rejectValue("dataInicio", datasInvalidas.getMessage(), datasInvalidas.getMessage());
			return novo(promocao);
		} catch (DataInicioPromocaoInvalida dataInicioInvalida) {
			result.rejectValue("dataInicio", dataInicioInvalida.getMessage(), dataInicioInvalida.getMessage());
			return novo(promocao);
		}
		
		attributes.addFlashAttribute("mensagem", "Promoção salva com sucesso!");
		return new ModelAndView("redirect:/promocoes/nova");
	}
	
	@GetMapping
	public ModelAndView pesquisar(PromocaoFilter promocaoFilter, @PageableDefault(size = 3) Pageable pageable, HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView("promocao/PesquisaPromocao");
		PageWrapper<Promocao> paginaWrapper = new PageWrapper<>(this.promocoes.pesquisar(promocaoFilter, pageable), httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		
		return mv;
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("id") Promocao promocao) {
		this.cadastroPromocaoService.excluir(promocao);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{id}")
	public ModelAndView editar(@PathVariable("id") Promocao promocao) {
		ModelAndView mv = novo(promocao);
		mv.addObject(promocao);
		return mv;
	}
}
