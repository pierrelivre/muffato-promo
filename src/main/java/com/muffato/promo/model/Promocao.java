package com.muffato.promo.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.util.StringUtils;

import com.muffato.promo.repository.listener.PromocaoEntityListener;

@Entity
@Table(name = "promocao")
@EntityListeners(PromocaoEntityListener.class)
public class Promocao implements Serializable {
	
	private static final long serialVersionUID = 2234255980151998222L;
	public static final String PFX = "prmc_";
	
	private Long id;
	private String nome;
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private String nomeArquivo;
	private String contentType;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = PFX + "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Size(max = 80, message = "O número máximo de caracteres do campo Nome é de 80!")
	@NotBlank(message = "Favor preencher o campo Nome!")
	@Column(name = PFX + "nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@NotNull(message = "Favor preencher o campo de Data de Início da Promoção!")
	@Column(name = PFX + "data_inicio")
	public LocalDate getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	@NotNull(message = "Favor preencher o campo de Data do Fim da Promoção!")
	@Column(name = PFX + "data_fim")
	public LocalDate getDataFim() {
		return dataFim;
	}
	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}
	
	@Column(name = PFX + "nome_arquivo")
	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	
	@Column(name = PFX + "content_type")
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	@Transient
	public boolean isNova() {
		return this.id == null;
	}
	
	@Transient
	public boolean isNotFile() {
		return StringUtils.isEmpty(this.nomeArquivo);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promocao other = (Promocao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
}
