package com.muffato.promo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.muffato.promo.model.Promocao;
import com.muffato.promo.repository.helper.promocoes.PromocoesQueries;

@Repository
public interface Promocoes extends JpaRepository<Promocao, Long>, PromocoesQueries {
	
	public List<Promocao> findByNomeIgnoreCase(String nome);
}
