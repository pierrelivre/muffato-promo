package com.muffato.promo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.muffato.promo.model.Usuario;

@Repository
public interface Usuarios extends JpaRepository<Usuario, Long> { 
	
	public Optional<Usuario> findByNome(String nome);

}
