package com.muffato.promo.repository.helper.promocoes;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.StringUtils;

import com.muffato.promo.model.Promocao;
import com.muffato.promo.repository.filter.PromocaoFilter;

public class PromocoesImpl implements PromocoesQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Promocao> pesquisar(PromocaoFilter filter, Pageable pageable) {
		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		CriteriaQuery<Promocao> criteria = builder.createQuery(Promocao.class);
		Root<Promocao> root = criteria.from(Promocao.class);
		
		Predicate[] predicates = criarRestricoes(filter, builder, root);
		criteria.where(predicates);
		this.ordenacao(builder, criteria, root, pageable, "nome");
		
		TypedQuery<Promocao> query = this.manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(filter));
	}

	private Predicate[] criarRestricoes(PromocaoFilter filter, CriteriaBuilder builder, Root<Promocao> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(filter.getNome())) {
			predicates.add(builder.like(
					builder.lower(root.get("nome")), 
					"%" + filter.getNome().toLowerCase() + "%"));
		}
		
		if (filter.getDataInicio() != null) {
			predicates.add(builder.greaterThanOrEqualTo(root.get("dataInicio"), filter.getDataInicio()));
		}
		
		if (filter.getDataFim() != null) {
			predicates.add(builder.lessThanOrEqualTo(root.get("dataFim"), filter.getDataFim()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	// Paginação
	private void adicionarRestricoesDePaginacao(TypedQuery<Promocao> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private void ordenacao(CriteriaBuilder builder, CriteriaQuery<?> criteria, Root<?> root, Pageable pageable, String propriedade) {
		// Ordenação
		Sort sort = pageable.getSort();
		if (sort != null && sort.isSorted()) {
			Order order = sort.iterator().next();
			String field = order.getProperty();
			criteria.orderBy(order.isAscending() ? builder.asc(root.get(field)) : builder.desc(root.get(field)));
		} else if (!StringUtils.isEmpty(propriedade)) {
			criteria.orderBy(builder.asc(root.get(propriedade)));
		}
	}
	
	private Long total(PromocaoFilter filter) {
		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Promocao> root = criteria.from(Promocao.class);
		
		Predicate[] predicates = criarRestricoes(filter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return this.manager.createQuery(criteria).getSingleResult();
	}
	
}
