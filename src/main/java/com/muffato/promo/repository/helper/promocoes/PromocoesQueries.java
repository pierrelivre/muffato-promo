package com.muffato.promo.repository.helper.promocoes;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.muffato.promo.model.Promocao;
import com.muffato.promo.repository.filter.PromocaoFilter;

public interface PromocoesQueries {

	public Page<Promocao> pesquisar(PromocaoFilter filter, Pageable pageable);
}
