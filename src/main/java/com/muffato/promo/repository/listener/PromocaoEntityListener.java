package com.muffato.promo.repository.listener;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import org.springframework.util.StringUtils;

import com.muffato.promo.MuffatoPromoApplication;
import com.muffato.promo.model.Promocao;
import com.muffato.promo.storage.ArquivoStorage;

public class PromocaoEntityListener {
	
	@PostUpdate
	@PostPersist
	public void postPersist(final Promocao promocao) {
		ArquivoStorage arquivoStorage = MuffatoPromoApplication.getBean(ArquivoStorage.class);
		if (!StringUtils.isEmpty(promocao.getNomeArquivo())) {
			arquivoStorage.salvar(promocao.getNomeArquivo());
		}
	}
}
