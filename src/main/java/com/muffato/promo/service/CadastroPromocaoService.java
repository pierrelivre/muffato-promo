package com.muffato.promo.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.muffato.promo.model.Promocao;
import com.muffato.promo.repository.Promocoes;
import com.muffato.promo.service.event.promocao.PromocaoExcluirEvent;
import com.muffato.promo.service.exception.DataInicioPromocaoInvalida;
import com.muffato.promo.service.exception.DatasPromocaoInvalidas;
import com.muffato.promo.service.exception.NomePromocaoJaCadastradoException;

@Service
public class CadastroPromocaoService {

	@Autowired
	private Promocoes promocoes;
	@Autowired
	private ApplicationEventPublisher publisher;
		
	@Transactional
	public void salvar(Promocao promocao) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
		List<Promocao> promocoesCadastrada = this.promocoes.findByNomeIgnoreCase(promocao.getNome());
		if (promocoesCadastrada != null && promocoesCadastrada.size() > 0 && promocao.isNova()) {
			for (Promocao promocaoCadastrada : promocoesCadastrada) {
				if (promocaoCadastrada.getDataInicio().equals(promocao.getDataInicio()) &&
						promocaoCadastrada.getDataFim().equals(promocao.getDataFim())) {
					throw new NomePromocaoJaCadastradoException("Já existe uma promoção cadastrada com o nome \"" + promocaoCadastrada.getNome() + 
							"\" para as datas de " + promocaoCadastrada.getDataInicio().format(formatter) + " até  " + promocaoCadastrada.getDataFim().format(formatter) + "!");
				}
			}
		}
		
		if (promocao.getDataInicio().isBefore(LocalDate.now()) && promocao.isNova()) {
			throw new DataInicioPromocaoInvalida("A data de início da promoção não pode ser menor que a data atual!");
		}
		
		if (promocao.getDataInicio().isAfter(promocao.getDataFim())) {
			throw new DatasPromocaoInvalidas("A data inícial da promoção não pode ser maior que a data final!");
		}
				
		this.promocoes.saveAndFlush(promocao);
	}

	@Transactional
	public void excluir(Promocao promocao) {
		String nomeArquivo = promocao.getNomeArquivo();
		this.promocoes.delete(promocao);
		this.promocoes.flush();
		this.publisher.publishEvent(new PromocaoExcluirEvent(nomeArquivo));
	}
}
