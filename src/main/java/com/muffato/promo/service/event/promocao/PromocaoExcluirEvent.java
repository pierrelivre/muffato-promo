package com.muffato.promo.service.event.promocao;

import org.springframework.util.StringUtils;

public class PromocaoExcluirEvent {
	
	private String nomeArquivo;

	public PromocaoExcluirEvent(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	
	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public boolean isFile() {
		return !StringUtils.isEmpty(nomeArquivo);
	}
}
