package com.muffato.promo.service.event.promocao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.muffato.promo.storage.ArquivoStorage;

@Component
public class PromocaoListener {

	@Autowired
	private ArquivoStorage arquivoStorage;
	
	@EventListener(condition = "#evento.file")
	public void promocaoExcluir(PromocaoExcluirEvent evento) {
		this.arquivoStorage.excluirArquivo(evento.getNomeArquivo());
	}
}
