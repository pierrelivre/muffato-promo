package com.muffato.promo.service.event.promocao;

import org.springframework.util.StringUtils;

import com.muffato.promo.model.Promocao;

public class PromocaoSalvaEvent {
	
	private Promocao promocao;

	public PromocaoSalvaEvent(Promocao promocao) {
		this.promocao = promocao;
	}

	public Promocao getPromocao() {
		return promocao;
	}
	
	public boolean isFile() {
		return !StringUtils.isEmpty(this.promocao.getNomeArquivo());
	}
}
