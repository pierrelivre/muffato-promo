package com.muffato.promo.service.exception;

public class DataInicioPromocaoInvalida extends RuntimeException {
	
	private static final long serialVersionUID = -8172461715672433025L;

	public DataInicioPromocaoInvalida(String message) {
		super(message);
	}
}
