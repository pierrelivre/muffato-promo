package com.muffato.promo.service.exception;

public class DatasPromocaoInvalidas extends RuntimeException {

	private static final long serialVersionUID = -7308665496240190692L;

	public DatasPromocaoInvalidas(String message) {
		super(message);
	}
}
