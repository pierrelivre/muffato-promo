package com.muffato.promo.service.exception;

public class NomePromocaoJaCadastradoException extends RuntimeException {

	private static final long serialVersionUID = 4725195160364868049L;

	public NomePromocaoJaCadastradoException(String message) {
		super(message);
	}
}
