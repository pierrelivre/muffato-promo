package com.muffato.promo.storage;

import org.springframework.web.multipart.MultipartFile;

public interface ArquivoStorage {

	public String salvarTemporariamente(MultipartFile[] files);

	public byte[] recuperarArquivoTemporario(String nome);

	public void salvar(String nomeArquivo);

	public byte[] recuperarArquivo(String nome);
	
	public void excluirArquivo(String nomeArquivo);
}
