package com.muffato.promo.storage.local;

import static java.nio.file.FileSystems.getDefault;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.muffato.promo.storage.ArquivoStorage;

@Component
public class ArquivoStorageLocal implements ArquivoStorage {

	private static final Logger LOG = LoggerFactory.getLogger(ArquivoStorageLocal.class);
	
	private Path local;
	private Path localTemporario;
	
	public ArquivoStorageLocal() {
		this(getDefault().getPath(System.getProperty("user.home"), ".muffatofiles"));
	}

	public ArquivoStorageLocal(Path path) {
		this.local = path;
		criarPastas();
	}
	
	@Override
	public String salvarTemporariamente(MultipartFile[] files) {
		String novoNome = null;
		if (files != null && files.length > 0) {
			MultipartFile arquivo = files[0];
			try {
				novoNome = renomarArquivo(arquivo.getOriginalFilename());
				arquivo.transferTo(new File(this.localTemporario.toAbsolutePath().toString() + getDefault().getSeparator() + novoNome));
			} catch (IOException e) {
				throw new RuntimeException("Erro salvando arquivo na pasta temporária", e);
			}
		}
		return novoNome;
	}
	
	@Override
	public void salvar(String nomeArquivo) {
		try {
			Files.move(this.localTemporario.resolve(nomeArquivo), this.local.resolve(nomeArquivo));
		} catch (IOException e) {
			throw new RuntimeException("Erro movendo arquivo para destino final");
		}
	}
	
	@Override
	public byte[] recuperarArquivoTemporario(String nome) {
		try {
			return Files.readAllBytes(this.localTemporario.resolve(nome));
		} catch (IOException e) {
			throw new RuntimeException("Erro lendo arquivo temporario", e);
		}
	}
	
	@Override
	public byte[] recuperarArquivo(String nome) {
		try {
			return Files.readAllBytes(this.local.resolve(nome));
		} catch (IOException e) {
			throw new RuntimeException("Erro lendo arquivo", e);
		}
	}

	@Override
	public void excluirArquivo(String nomeArquivo) {
		try {
			Files.deleteIfExists(this.local.resolve(nomeArquivo));
		} catch (IOException e) {
			LOG.warn(String.format("Erro excluindo arquivo '%s'. Mensagem: %s", nomeArquivo, e.getMessage()));
		}
	}
	
	private void criarPastas() {
		try {
			Files.createDirectories(this.local);
			this.localTemporario = getDefault().getPath(this.local.toString(), "temp");
			Files.createDirectories(this.localTemporario);	
			
			if (LOG.isDebugEnabled()) {
				LOG.debug("Pastas para salvar arquivos criadas com sucesso!");
				LOG.debug("Pasta default: " + this.local.toAbsolutePath());
				LOG.debug("Pasta temporária: " + this.localTemporario.toAbsolutePath());
			}
		} catch (IOException e) {
			throw new RuntimeException("Erro criando pasta para salvar os arquivos", e);
		}
	}
	
	private String renomarArquivo(String nomeOriginal) {
		String novoNome = UUID.randomUUID().toString().concat("_").concat(nomeOriginal);
		
		if (LOG.isDebugEnabled()) {
			LOG.debug(String.format("Nome original: %s, Novo nome do arquivo %s", nomeOriginal, novoNome));
		}
		
		return novoNome;
	}
}
