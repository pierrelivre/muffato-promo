package com.muffato.promo.thymeleaf;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import com.muffato.promo.thymeleaf.processor.ClassForErrorAttributeTagProcessor;
import com.muffato.promo.thymeleaf.processor.MenuAttributeTagProcessor;
import com.muffato.promo.thymeleaf.processor.MessageElementTagProcessor;
import com.muffato.promo.thymeleaf.processor.OrderElementTagProcessor;
import com.muffato.promo.thymeleaf.processor.PaginationElementTagProcessor;

@Component
public class MuffatoDialect extends AbstractProcessorDialect {

	protected MuffatoDialect() {
		super("Muffato", "mf", StandardDialect.PROCESSOR_PRECEDENCE);
	}

	@Override
	public Set<IProcessor> getProcessors(String dialectPrefix) {
		final Set<IProcessor> processadores = new HashSet<>();
		processadores.add(new MessageElementTagProcessor(dialectPrefix));
		processadores.add(new ClassForErrorAttributeTagProcessor(dialectPrefix));
		processadores.add(new PaginationElementTagProcessor(dialectPrefix));
		processadores.add(new OrderElementTagProcessor(dialectPrefix));
		processadores.add(new MenuAttributeTagProcessor(dialectPrefix));
		return processadores;
	}
}
