CREATE TABLE usuario (
	usro_id BIGINT(20) PRIMARY KEY,
	usro_nome VARCHAR(50) NOT NULL,
	usro_senha VARCHAR(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE permissao (
	prms_id BIGINT(20) PRIMARY KEY,
	prms_descricao VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuario_permissao (
	uspr_usuario_id BIGINT(20) NOT NULL,
	uspr_permissao_id BIGINT(20) NOT NULL,
	PRIMARY KEY (uspr_usuario_id, uspr_permissao_id),
	FOREIGN KEY (uspr_usuario_id) REFERENCES usuario(usro_id),
	FOREIGN KEY (uspr_permissao_id) REFERENCES permissao(prms_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO usuario (usro_id, usro_nome, usro_senha) values (1, 'Administrador', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.');
INSERT INTO usuario (usro_id, usro_nome, usro_senha) values (2, 'Mariane', '$2a$10$eWH6Js4ZYu08Lq/nTG6a2ugXUlOlLLweBuVaSa1QqLkY6BarUpzIW');

INSERT INTO permissao (prms_id, prms_descricao) values (1, 'ROLE_CADASTRAR_PROMOCAO');
INSERT INTO permissao (prms_id, prms_descricao) values (2, 'ROLE_PESQUISAR_PROMOCAO');
INSERT INTO permissao (prms_id, prms_descricao) values (3, 'ROLE_REMOVER_PROMOCAO');
INSERT INTO permissao (prms_id, prms_descricao) values (4, 'ROLE_EDITAR_PROMOCAO');
                      
-- admin
INSERT INTO usuario_permissao (uspr_usuario_id, uspr_permissao_id) values (1, 1);
INSERT INTO usuario_permissao (uspr_usuario_id, uspr_permissao_id) values (1, 2);
INSERT INTO usuario_permissao (uspr_usuario_id, uspr_permissao_id) values (1, 3);
INSERT INTO usuario_permissao (uspr_usuario_id, uspr_permissao_id) values (1, 4);
                               
-- mariane       
INSERT INTO usuario_permissao (uspr_usuario_id, uspr_permissao_id) values (2, 2);
INSERT INTO usuario_permissao (uspr_usuario_id, uspr_permissao_id) values (2, 4);