var Muffato = Muffato || {};

Muffato.UploadAqruivo = (function() {
	
	function UploadAqruivo() {
		this.inputNomeArquivo = $('input[name=nomeArquivo]');
		this.inputContentType = $('input[name=contentType]');
		
		this.htmlArquivoUploadTemplate = $('#arquivo-upload').html();
		this.template = Handlebars.compile(this.htmlArquivoUploadTemplate);
		
		this.containerArquivoUpload = $('.js-container-arquivo');
		
		this.uploadDrop = $('#upload-drop');
		this.imgLoading = $('.js-img-loading');
		
		this.botaoRemoverArquivo;
	}
	
	UploadAqruivo.prototype.iniciar = function() {
		var settings = {
			type: 'json',
			filelimit: 1,
			allow: '*.(csv)',
			action: this.containerArquivoUpload.data('url-arquivo'),
			complete: onUploadCompleto.bind(this),
			loadstart: onLoadStart.bind(this),
			notallowed: onErrorExtensaoArquivo.bind(this)
		}
		
		UIkit.uploadSelect($('#upload-select'), settings);
		UIkit.uploadDrop(this.uploadDrop, settings);

		if (this.inputNomeArquivo.val()) {
			renderizarFoto.call(this, { 
				nome:  this.inputNomeArquivo.val(), 
				contentType: this.inputContentType.val()});
		}
	}
	
	function onErrorExtensaoArquivo() {
		swal({
			title: 'Ops!',
			text: 'Não é possível importar este arquivo, somente arquivos com extensão .csv!',
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Ok',
			closeOnConfirm: false
		});
	}
	
	function onLoadStart() {
		this.imgLoading.removeClass('hidden');
	}
		
	function onUploadCompleto(resposta) {
		this.imgLoading.addClass('hidden');		
		renderizarFoto.call(this, resposta);
	}
	
	function renderizarFoto(resposta) {
		this.inputNomeArquivo.val(resposta.nome);
		this.inputContentType.val(resposta.contentType);
		
		this.uploadDrop.addClass('hidden');
		
		var htmlArquivoUpload = this.template({ nomeArquivo: resposta.nome });
		this.containerArquivoUpload.append(htmlArquivoUpload);
		
		$('.js-remove-arquivo').on('click', onRemoverArquivo.bind(this))
	}
	
	function onRemoverArquivo() {
		$('.js-arquivo-upload').remove();
		this.uploadDrop.removeClass('hidden');
		this.inputNomeArquivo.val('');
		this.inputContentType.val('');
	}
	
	return UploadAqruivo;
})();

$(function() {
	var uploadArquivo = new Muffato.UploadAqruivo();
	uploadArquivo.iniciar();
});